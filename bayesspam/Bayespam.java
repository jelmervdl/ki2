import java.io.*;
import java.util.*;

public class Bayespam
{
	// StringUtil-like class (yeah, it's kind of inspired on the Apache 
	// foundation one) to replace the regex methods I used. For the supplied
	// training and test sets it results in a speed-up of about 40%! Those
	// regex functions are really expensive aparently.
	static class StringUtil
	{
		// Replacement for String.matches("\\d")
		// Scientific notations like 1.0e4 is not detected
		static public boolean isNumeric(String word)
		{
			for (int i = 0; i < word.length(); ++i)
				if (!Character.isDigit(word.charAt(i)))
					return false;
			
			// If only digits where encountered, this is a number.
			return true;
		}
		
		// Replacement for String.replaceAll("[^a-z0-9]", "")
		static public String stripNonAlphaNumeric(String word)
		{
			StringBuffer chars = new StringBuffer(word.length());
			
			// Recreate the string, but only add chars that are letters or numbers, no punctuation
			for (int i = 0; i < word.length(); ++i)
				if (Character.isDigit(word.charAt(i)) || Character.isLetter(word.charAt(i)))
					chars.append(word.charAt(i));
			
			return chars.toString();
		}
	}
	
	// SpamCounter is added to the vocabulary to keep track of the occurences
	// of the word in the training set.
	static class SpamCounter
	{
		public int countedAsSpam;
		
		public int countedAsRegular;
		
		public SpamCounter(int spam, int regular)
		{
			countedAsSpam = spam;
			countedAsRegular = regular;
		}
		
		public void markSpam()
		{
			++countedAsSpam;
		}
		
		public void markRegular()
		{
			++countedAsRegular;
		}
		
		public int occurrences()
		{
			return countedAsSpam + countedAsRegular;
		}

		public double isRegularChance()
		{
			return (double) countedAsRegular / occurrences();
		}

		public double isSpamChance()
		{
			return (double) countedAsSpam / occurrences();
		}
	}
	
	static class NGram
	{
		private String[] words;

		public NGram(String[] words)
		{
			this.words = words;
		}

		public boolean equals(Object other)
		{
			for (int i = 0; i < words.length; ++i)
				if (!words[i].equals(((NGram) other).words[i]))
					return false;

			return true;
		}

		public int hashCode()
		{
			int hashCode = 0;

			for (int i = 0; i < words.length; ++i)
				hashCode += words[i].hashCode();

			return hashCode;
		}
		
		// This is handy for printVocabulary(...)
		public String toString()
		{
			return Arrays.toString(words);
		}
	}
	
	// The tokenizer splits an email into ngrams, ignoring non-alphanumeric
	// characters and short words.
	static class Tokenizer
	{
		private int ngramSize;

		public Tokenizer(int ngramSize)
		{
			this.ngramSize = ngramSize;
		}

		public Vector<NGram> tokenize(File email) throws IOException
		{
			FileInputStream istream = new FileInputStream(email);
			InputStreamReader istreamReader = new InputStreamReader(istream);
			BufferedReader buffer = new BufferedReader(istreamReader);

			Vector<NGram> words = new Vector<NGram>();

			String line;
			Queue<String> previousTokens = new LinkedList<String>();

			while ((line = buffer.readLine()) != null)
			{
				StringTokenizer tokenizer = new StringTokenizer(line);

				while (tokenizer.hasMoreTokens())
				{
					// Strip non-alphanumeric chars and convert to lower case.
					String word = StringUtil.stripNonAlphaNumeric(
						tokenizer.nextToken()).toLowerCase();

					// Ignore every numeral (but don't ignore stuff like pr0n)
					if (StringUtil.isNumeric(word))
						continue;

					// Ignore words smaller than 4 characters
					if (word.length() < MINIMAL_WORD_SIZE)
						continue;

					// Add the token to the queue
					previousTokens.add(word);

					// If we have enough tokens in the queue to create a NGram, do so.
					if (previousTokens.size() == ngramSize)
					{
						words.add(new NGram(previousTokens.toArray(new String[] {})));

						// Make room for the next token.
						previousTokens.remove();
					}
				}
			}

			return words;
		}
	}
	
	// SpamKnowledge stores the probabilities for each word and calculates
	// all the chances and likelyhoods. It contains the judgement algorithm.
	static class SpamKnowledge
	{
		public Hashtable<NGram, SpamCounter> vocabulary;
		
		private double pRegular;
		
		private double pSpam;
		
		private double lWordFallback;
		
		private int nWordsRegular;
		
		private int nWordsSpam;
		
		private int nWords;

		public SpamKnowledge(Hashtable<NGram, SpamCounter> vocab, int nMailsSpam, int nMailsRegular)
		{
			vocabulary = vocab;
			
			// A priory knowledge:
			
			// The probability any random message is ham.
			pRegular = (double) nMailsRegular / (nMailsSpam + nMailsRegular);
			
			// And the probability this unseen message is spam.
			pSpam = (double) nMailsSpam / (nMailsSpam + nMailsRegular);
			
			// Total number of words in both categories, used to calculate P(w|spam/ham)
			for (SpamCounter counter : vocabulary.values())
			{
				nWordsRegular += counter.countedAsRegular;
				nWordsSpam += counter.countedAsSpam;
			}
			
			// The total number of words found in all the training data
			nWords = nWordsSpam + nWordsRegular;

			// Calculate the fallback likelihood
			lWordFallback = Math.log(ETHA / nWords);
		}
		
		// arg max (P(Spam|Words), P(Regular|Words))
		public boolean isSpam(Collection<NGram> words)
		{
			return isSpamLikelyhood(words) > isRegularLikelyhood(words);
		}
		
		// log(aP(regular) * P(w1|regular) * P(wn|regular))
		private double isRegularLikelyhood(Collection<NGram> words)
		{
			double likelyhood = Math.log(pRegular);
			
			for (NGram word : words)
				likelyhood += isRegularWordLikelyhood(word);

			return likelyhood;
		}

		// log(aP(spam) * P(w1|spam) * P(wn|spam))
		private double isSpamLikelyhood(Collection<NGram> words)
		{
			double likelyhood = Math.log(pSpam);
			
			for (NGram word : words)
				likelyhood += isSpamWordLikelyhood(word);
			
			return likelyhood;
		}
		
		// logP(w|regular) = log(P(regular|w) * P(w) / P(regular))
		private double isRegularWordLikelyhood(NGram word)
		{
			SpamCounter wordStats = vocabulary.get(word);

			if (wordStats == null || wordStats.countedAsRegular == 0)
				return lWordFallback;

			return wordStats.isRegularChance() * ((double) wordStats.occurrences() / nWords) / pRegular;
			// return Math.log(wordStats.isRegularChance() * ((double) wordStats.occurrences() / nWords) / pRegular);
		}
		
		// logP(w|spam) = log(P(spam|w) * P(w) / P(spam))
		private double isSpamWordLikelyhood(NGram word)
		{
			SpamCounter wordStats = vocabulary.get(word);

			if (wordStats == null || wordStats.countedAsSpam == 0)
				return lWordFallback;

			return wordStats.isSpamChance() * ((double) wordStats.occurrences() / nWords) / pSpam;
			// return Math.log(wordStats.isSpamChance() * ((double) wordStats.occurrences() / nWords) / pSpam);
		}
	}
	
	// Class to wrap a combination of spam and regular mails. Typical
	// datasets should be the training and test set.
	static class DataSet
	{
		public File[] regular;
		
		public File[] spam;
		
		public DataSet(File[] regularMail, File[] spamMail)
		{
			regular = regularMail;
			spam = spamMail;
		}
	}
	
	// The test stage uses a confusion matrix to store the test score. This
	// is such a matrix. It sounds a lot more complex than it really is.
	static class ConfusionMatrix
	{
		public int truePositives;
		
		public int falsePositives;
		
		public int trueNegatives;
		
		public int falseNegatives;
		
		public String toString()
		{
			return "True positives: " + truePositives + "\t"
				 + "False positives: " + falsePositives + "\n"
				 + "False negatives: " + falseNegatives + "\t"
				 + "True negatives: " + trueNegatives;
		}
	}
	
	// Find the spam mail files and regular mail files in a directory and
	// return these two lists of files in a DataSet.
	static private DataSet loadEmail(File parent) throws RuntimeException
	{
		// Check if the cmd line arg is a directory and list it
		if (!parent.isDirectory())
			throw new RuntimeException("Error: cmd line arg not a directory.");
		
		// Listings of the two subdirectories (regular/ and spam/)
		File[] regularMail = new File[0];
		File[] spamMail = new File[0];
		
		// Find the regular and test directories inside the provided directory
		for(File dir : parent.listFiles())
		{
			// Skip system files etc.
			if (!dir.isDirectory())
				continue;
			
			if (dir.getName().equals("regular"))
				regularMail = dir.listFiles();
			
			else if (dir.getName().equals("spam"))
				spamMail = dir.listFiles();
		}
		
		// If not both a regular and a spam directory are found, bail out.
		if (regularMail.length == 0 || spamMail.length == 0)
			throw new RuntimeException("Error: provided directory does not contain a regular and a spam directory");
		
		return new DataSet(spamMail, regularMail);
	}
	
	// Helper function to print the vocabulary of ngrams and its counts
	// (used in verbose output)
	static private void printVocabulary(SpamKnowledge knowledge)
	{
		for (NGram word : knowledge.vocabulary.keySet())
		{
			SpamCounter counter = knowledge.vocabulary.get(word);
			System.out.println(word + "\t" + counter.countedAsSpam + "\t" + counter.countedAsRegular);
		}
	}
	
	// Training fase of the spam filter. Read all the files in the training 
	// set, read every word, and count it as spam/regular.
	static private SpamKnowledge train(DataSet trainingSet) throws IOException
	{
		// Store all the "words" in a hashtable next to their statistics.
		Hashtable<NGram, SpamCounter> vocabulary = new Hashtable<NGram, SpamCounter>();
		
		// Read all the words in the regular mails, and count them as non-spam.
		for (File email : trainingSet.regular)
		{
			for (NGram word : tokenizer.tokenize(email))
			{
				if (!vocabulary.containsKey(word))
					vocabulary.put(word, new SpamCounter(0, 1));
				else
					vocabulary.get(word).markRegular();
			}
		}
		
		// Now do the same thing for the spam mail, but count them as spam.
		for (File email : trainingSet.spam)
		{
			for (NGram word : tokenizer.tokenize(email))
			{
				if (!vocabulary.containsKey(word))
					vocabulary.put(word, new SpamCounter(1, 0));
				else
					vocabulary.get(word).markSpam();
			}
		}
		
		return new SpamKnowledge(vocabulary, trainingSet.spam.length, trainingSet.regular.length);
	}
	
	// Prune the vocabulary. Or, more or less, create a new, filtered one
	// because you can't remove elements from a hashtable when iterating over
	// all it's elements.
	static private void prune(SpamKnowledge knowledge)
	{
		Hashtable<NGram, SpamCounter> cleanVocabulary = new Hashtable<NGram, SpamCounter>();
		
		for (NGram word : knowledge.vocabulary.keySet())
		{
			SpamCounter counter = knowledge.vocabulary.get(word);
			if (counter.occurrences() >= NGRAM_OCCURRENCE_THRESHOLD)
				cleanVocabulary.put(word, counter);
		}
		
		knowledge.vocabulary = cleanVocabulary;
	}
	
	// Using the data obtained from train(), test the knowledge and algorithm
	// by trying to judge de emails in testSet correctly.
	static private ConfusionMatrix test(DataSet testSet, SpamKnowledge knowledge) throws IOException
	{
		ConfusionMatrix result = new ConfusionMatrix();
		
		for (File email : testSet.spam)
		{
			if (knowledge.isSpam(tokenizer.tokenize(email)))
				++result.truePositives;
			else
				++result.falseNegatives;
		}
		
		for (File email : testSet.regular)
		{
			if (knowledge.isSpam(tokenizer.tokenize(email)))
				++result.falsePositives;
			else
				++result.trueNegatives;
		}
		
		return result;
	}
	
	// The Tokenizer splits an email into ngrams. The size of the ngram is
	// passed as argument to the constructor. For example: bigrams have a
	// size of 2.
	static private Tokenizer tokenizer = new Tokenizer(2);
	
	// Etha, used in the fallback probability when a ngram is not known in the
	// vocabulary.
	static double ETHA = 1.0d;
	
	// Minimal word size, used by the tokenizers to only count words of equal
	// length or longer.
	static int MINIMAL_WORD_SIZE = 4;
	
	// Every ngram should at least have been encountered N times.
	static int NGRAM_OCCURRENCE_THRESHOLD = 3;
	
	static boolean BE_VERBOSE = false;
	
	// Everything happens here, really.
	public static void main(String[] args) throws IOException
	{
		// Parse program arguments and return argument offset of the file names.
		int idx = parseArguments(args);
		
		// Scan all the emails in the train directory.
		DataSet trainingSet = loadEmail(new File(args[idx]));
		
		// Train using this dataset and save the knowledge.
		SpamKnowledge knowledge = train(trainingSet);
		
		if (BE_VERBOSE)
			printVocabulary(knowledge);
		
		System.out.println(knowledge.vocabulary.size() + " ngrams in the vocabulary");
		
		// Prune the vocabulary, removing all the ngrams that don't occur often enough
		prune(knowledge);
		
		System.out.println(knowledge.vocabulary.size() + " ngrams in the vocabulary after pruning");
		
		// Scan all the emails in the test directory.
		DataSet testSet = loadEmail(new File(args[++idx]));
		
		// Test the trained knowledge using the emails in the test set.
		ConfusionMatrix result = test(testSet, knowledge);
		
		// Now print the confusion matrix.
		System.out.println(result);
	}
	
	
	// Parses program parameters and returns argument offset of the file names.
	static private int parseArguments(String[] args) throws RuntimeException
	{
		int idx = -1;
		while (++idx < args.length)
		{
			if (args[idx].equals("--word-size-th"))
			{
				MINIMAL_WORD_SIZE = Integer.parseInt(args[++idx]);
				continue;
			}
			
			if (args[idx].equals("--occurrence-th"))
			{
				NGRAM_OCCURRENCE_THRESHOLD = Integer.parseInt(args[++idx]);
				continue;
			}
			
			if (args[idx].equals("--ngram"))
			{
				tokenizer = new Tokenizer(Integer.parseInt(args[++idx]));
				continue;
			}
			
			if (args[idx].equals("--verbose"))
			{
				BE_VERBOSE = true;
				continue;
			}
			
			break;
		}
		
		// If not enough arguments are supplied, print usage message
		if (args.length < idx + 2)
		{
			System.out.println("java Bayespam [--word-size-th n] [--occurrence-th n] [--ngram n] [--verbose] train-dir test-dir");
			Runtime.getRuntime().exit(0);
		}
		
		return idx;
	}
}
