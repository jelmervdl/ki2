import java.util.*;

public class Kmeans extends ClusteringAlgorithm
{
    // Number of clusters
    private int k;

    // Dimensionality of the vectors
    private int dim;
    
    // Threshold above which the corresponding html is prefetched
    private double prefetchThreshold;

    // array of k clusters, class cluster is used for easy bookkeeping
    private Cluster[] clusters;
    
    // This class represents the clusters, it contains the prototype (the mean of all it's members)
    // and memberlists with the ID's (which are Integer objects) of the datapoints that are member of that cluster.
    // You also want to remember the previous members so you can check if the clusters are stable.
    static class Cluster
    {
         float[] prototype;
         Vector<Integer> currentMembers;
         Vector<Integer> previousMembers;
 
         public Cluster()
         {
            currentMembers = new Vector<Integer>();
            previousMembers = new Vector<Integer>();
         }
    }
    // These vectors contains the feature vectors you need; the feature vectors are float arrays.
    // Remember that you have to cast them first, since vectors return objects.
    private Vector<float[]> trainData;
    private Vector<float[]> testData;
    // Results of test()
    private double hitrate;
    private double accuracy;
    
    public Kmeans(int k, Vector<float[]> trainData, Vector<float[]> testData, int dim)
    {
        this.k = k;
        this.trainData = trainData;
        this.testData = testData; 
        this.dim = dim;
        prefetchThreshold = 0.5;

        // Here k new cluster are initialized
        clusters = new Cluster[k];
        for (int i = 0; i < k; i++)
            clusters[i] = new Cluster();
    }
 
 
    public boolean train()
    {
        //implement k-means algorithm here:

        // Step 1: Select an initial random partioning with k clusters
        Random randomIndexGenerator = new Random();
        for (int i = 0; i < trainData.size(); ++i)
        {
            int randomIndex = randomIndexGenerator.nextInt(k);
            clusters[randomIndex].currentMembers.add(i);
        }

        // Step 4: repeat until clustermembership stabilizes
        boolean stable = false;
        while (!stable)
        {
            // Be optimistic, and hope not a single member jumps clusters this iteration (making it the final iteration)
            stable = true;

            // Step 2: recalculate cluster centers (i.e. the cluster prototypes)
            for (int c = 0; c < k; ++c)
            {
                // reset prototypes
                clusters[c].prototype = new float[dim];

                // sum all the member float[]'s in the prototype float[].
                for (int m = 0; m < clusters[c].currentMembers.size(); ++m)
                {
                    for (int d = 0; d < dim; ++d)
                    {
                        int id = clusters[c].currentMembers.get(m);
                        clusters[c].prototype[d] += trainData.get(id)[d];
                    }
                }

                // divide all the floats in the prototype by the amount of members; now we have the averages
                for (int d = 0; d < dim; ++d)
                    clusters[c].prototype[d] /= clusters[c].currentMembers.size();
            
                // backup the members of all the clusters into the oldmembers vectors so the old and new
                // partitioning don't get mixed up.
                clusters[c].previousMembers = clusters[c].currentMembers;
                clusters[c].currentMembers = new Vector<Integer>();
            }

            // Step 3: Generate a new partition by assigning each data point to its closest cluster center
            // Walk through all the (old) members of all the clusters and assign them to the nearest
            for (int c = 0; c < k; ++c)
            {
                for (int m = 0; m < clusters[c].previousMembers.size(); ++m)
                {
                    // id of the data point (the index in the trainData vector)
                    int id = clusters[c].previousMembers.get(m);

                    // find the nearest cluster (assume it is the first, then iterate over the others to see if one is closer)
                    int nearestClusterIndex = 0; 
                    double nearestClusterDistance = distance(trainData.get(id), clusters[0].prototype);
                    for (int c2 = 1; c2 < k; ++c2)
                    {
                        double clusterDistance = distance(trainData.get(id), clusters[c2].prototype);

                        // if this cluster is closer, make this the nearest cluster for now
                        if (clusterDistance < nearestClusterDistance)
                        {
                            nearestClusterIndex = c2;
                            nearestClusterDistance = clusterDistance;
                        }
                    }

                    // assign this data point to the nearest cluster
                    clusters[nearestClusterIndex].currentMembers.add(id);

                    // if this means that the data point changed clusters, the partitioning isn't stable yet
                    if (nearestClusterIndex != c)
                        stable = false;
                }
            }
        }
        
	    return false;
    }
 
    public boolean test()
    {
        int prefetchedPagesRequested = 0;
        int pagesRequested = 0;
        int pagesPrefetched = 0;

        // iterate along all clients.
        // for each client find the cluster of which it is a member
        for (int c = 0; c < k; ++c)
        {
            for (int m = 0; m < clusters[c].currentMembers.size(); ++m)
            {   
                int id = clusters[c].currentMembers.get(m);

                // get the actual testData (the vector) of this client
                // Assumption: the same clients are in the same order as in the testData
                float[] actualRequests = testData.get(id);
                
                // iterate along all dimensions
                // and count prefetched htmls
                // count number of hits
                // count number of requests    
                for (int d = 0; d < dim; ++d)
                {
                    boolean prefetched = clusters[c].prototype[d] < prefetchThreshold;

                    if (prefetched)
                        pagesPrefetched++;

                    if (actualRequests[d] == 1.0)
                        pagesRequested++;

                    if (prefetched && actualRequests[d] == 1.0)
                        prefetchedPagesRequested++;
                }
            }
        }

        hitrate = (double) prefetchedPagesRequested / pagesRequested;

        accuracy = (double) prefetchedPagesRequested / pagesPrefetched;
        
        // set the global variables hitrate and accuracy to their appropriate value
        return true;
    }

    // Calculates the euclidian distance between an user and a prototype
    private double distance(float[] user, float[] prototype)
    {
        double square_sum = 0;

        for (int d = 0; d < dim; ++d)
        {
            double delta = user[d] - prototype[d];
            square_sum += delta * delta;
        }

        return Math.sqrt(square_sum);
    }
 
 
    // The following members are called by runClustering, in order to present information to the user
    public void showTest()
    {
        System.out.println("Prefetch threshold=" + this.prefetchThreshold);
        System.out.println("Hitrate: " + this.hitrate);
        System.out.println("Accuracy: " + this.accuracy);
        System.out.println("Hitrate+Accuracy=" + (this.hitrate + this.accuracy));
    }
    
    public void showMembers()
    {
         for (int i = 0; i < k; i++)
            System.out.println("\nMembers cluster[" + i + "] :" + clusters[i].currentMembers);
    }

    public void showPrototypes()
    {
        for (int i = 0; i < k; i++)
        {
            System.out.print("\nPrototype cluster[" + i + "] :");
            
            for (int i2 = 0; i2 < dim; i2++)
                System.out.print(clusters[i].prototype[i2] + " ");

            System.out.println();
        }
    }  
    // with this function you can set the prefetch threshold.
    public void setPrefetchThreshold(double prefetchThreshold)
    {
        this.prefetchThreshold = prefetchThreshold;
    }    
}
